/*************************
 * stm32 uart main.c
 *************************/

#include "stm32f10x.h"
#include "stm32f10x_conf.h"

/* User defined function prototypes */
void GPIOA_Init(void);
void USART1_Init(void);
void led_toggle(void);
void dma_write(char *data, uint32_t size);
void dma_read(char *data, uint32_t size);
void clock_setup();

volatile int transferred = 0;
volatile int received = 0;

int main(void) {
	char echo[5] = "echo ";
	char newLine[2] = "\n\r";
	char rx;
	char buffer[100];
	int index = 0;

	clock_setup();
	USART1_Init();
	GPIOA_Init();

	dma_write(&rx, 1);
	dma_read(&rx, 1);

	while (1) {
		while (transferred != 1) {
			if (received == 1) {
				received = 0;
				dma_read(&rx, 1);

				buffer[index++] = rx;

				if (rx == '\r') {
					transferred = 0;
					dma_write(newLine, 2);
					while (transferred != 1);

					transferred = 0;
					dma_write(echo, 5);
					while (transferred != 1);

					transferred = 0;
					dma_write(buffer, index);
					while (transferred != 1);

					transferred = 0;
					dma_write(newLine, 2);

					index = 0;
				} else {
					dma_write(&rx, 1);
				}

				if (index == 100)
					index = 0;
			}
		}
		transferred = 0;
	}
}

void clock_setup() {
	RCC_APB2PeriphClockCmd(
	RCC_APB2Periph_USART1 | RCC_APB2Periph_AFIO | RCC_APB2Periph_GPIOA, ENABLE);
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA1, ENABLE); // enable DMA clock
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
}

void dma_write(char *data, uint32_t size) {
	DMA_DeInit(DMA1_Channel4);

	DMA_InitTypeDef dma_init_struct;
	DMA_StructInit(&dma_init_struct);
	dma_init_struct.DMA_PeripheralBaseAddr = (uint32_t) &USART1->DR;
	dma_init_struct.DMA_MemoryBaseAddr = (uint32_t) data;
	dma_init_struct.DMA_BufferSize = size;
	dma_init_struct.DMA_DIR = DMA_DIR_PeripheralDST;
	dma_init_struct.DMA_Mode = DMA_Mode_Normal;
	dma_init_struct.DMA_MemoryInc = DMA_MemoryInc_Enable;
	dma_init_struct.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte;
	dma_init_struct.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;
	dma_init_struct.DMA_Priority = DMA_Priority_VeryHigh;

	DMA_Init(DMA1_Channel4, &dma_init_struct);
	DMA_ITConfig(DMA1_Channel4, DMA_IT_TC, ENABLE);
	DMA_Cmd(DMA1_Channel4, ENABLE);
	USART_DMACmd(USART1, USART_DMAReq_Tx, ENABLE);
}

void dma_read(char *data, uint32_t size) {
	DMA_DeInit(DMA1_Channel5);

	DMA_InitTypeDef dma_init_struct;
	DMA_StructInit(&dma_init_struct);
	dma_init_struct.DMA_PeripheralBaseAddr = (uint32_t) &USART1->DR;
	dma_init_struct.DMA_MemoryBaseAddr = (uint32_t) data;
	dma_init_struct.DMA_BufferSize = size;
	dma_init_struct.DMA_DIR = DMA_DIR_PeripheralSRC;
	dma_init_struct.DMA_Mode = DMA_Mode_Normal;
	dma_init_struct.DMA_MemoryInc = DMA_MemoryInc_Enable;
	dma_init_struct.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte;
	dma_init_struct.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;
	dma_init_struct.DMA_Priority = DMA_Priority_VeryHigh;

	DMA_Init(DMA1_Channel5, &dma_init_struct);
	DMA_ITConfig(DMA1_Channel5, DMA_IT_TC, ENABLE);
	DMA_Cmd(DMA1_Channel5, ENABLE);
	USART_DMACmd(USART1, USART_DMAReq_Rx, ENABLE);
}

/***********************************************
 * Initialize GPIOA PIN13 as push-pull output
 ***********************************************/
void GPIOA_Init(void) {
	/* Bit configuration structure for GPIOA PIN8 */
	GPIO_InitTypeDef gpioa_init_struct = { GPIO_Pin_13, GPIO_Speed_50MHz,
			GPIO_Mode_Out_PP };

	/* Enable PORT A clock */

	/* Initialize GPIOA: 50MHz, PIN8, Push-pull Output */
	GPIO_Init(GPIOA, &gpioa_init_struct);

	/* Turn off LED to start with */
	GPIO_SetBits(GPIOA, GPIO_Pin_13);
}

void USART1_Init(void) {
	/* USART configuration structure for USART1 */
	USART_InitTypeDef usart1_init_struct;
	/* Bit configuration structure for GPIOA PIN9 and PIN10 */
	GPIO_InitTypeDef gpioa_init_struct;

	/* GPIOA PIN9 alternative function Tx */
	gpioa_init_struct.GPIO_Pin = GPIO_Pin_9;
	gpioa_init_struct.GPIO_Speed = GPIO_Speed_50MHz;
	gpioa_init_struct.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_Init(GPIOA, &gpioa_init_struct);
	/* GPIOA PIN9 alternative function Rx */
	gpioa_init_struct.GPIO_Pin = GPIO_Pin_10;
	gpioa_init_struct.GPIO_Speed = GPIO_Speed_50MHz;
	gpioa_init_struct.GPIO_Mode = GPIO_Mode_IN_FLOATING;
	GPIO_Init(GPIOA, &gpioa_init_struct);

	/* Enable USART1 */
	USART_Cmd(USART1, ENABLE);
	/* Baud rate 115200, 8-bit data, One stop bit
	 * No parity, Do both Rx and Tx, No HW flow control
	 */
	usart1_init_struct.USART_BaudRate = 115200;
	usart1_init_struct.USART_WordLength = USART_WordLength_8b;
	usart1_init_struct.USART_StopBits = USART_StopBits_1;
	usart1_init_struct.USART_Parity = USART_Parity_No;
	usart1_init_struct.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
	usart1_init_struct.USART_HardwareFlowControl =
	USART_HardwareFlowControl_None;
	/* Configure USART1 */
	USART_Init(USART1, &usart1_init_struct);

	NVIC_EnableIRQ(DMA1_Channel4_IRQn);
	NVIC_EnableIRQ(DMA1_Channel5_IRQn);
}

void led_toggle(void) {
	/* Read LED output (GPIOA PIN8) status */
	uint8_t led_bit = GPIO_ReadOutputDataBit(GPIOA, GPIO_Pin_13);

	/* If LED output set, clear it */
	if (led_bit == (uint8_t) Bit_SET) {
		GPIO_ResetBits(GPIOA, GPIO_Pin_13);
	}
	/* If LED output clear, set it */
	else {
		GPIO_SetBits(GPIOA, GPIO_Pin_13);
	}
}

void DMA1_Channel4_IRQHandler(void) {
	if (DMA_GetITStatus(DMA1_IT_TC4) != RESET) {
		DMA_ClearITPendingBit(DMA1_IT_TC4);
		transferred = 1;
	}

	DMA_ITConfig(DMA1_Channel4, DMA_IT_TC, DISABLE);
	DMA_Cmd(DMA1_Channel4, DISABLE);
	USART_DMACmd(USART1, USART_DMAReq_Tx, DISABLE);
}

void DMA1_Channel5_IRQHandler(void) {
	if (DMA_GetITStatus(DMA1_IT_TC5) != RESET) {
		DMA_ClearITPendingBit(DMA1_IT_TC5);
		received = 1;
	}

	DMA_ITConfig(DMA1_Channel5, DMA_IT_TC, DISABLE);
	DMA_Cmd(DMA1_Channel5, DISABLE);
	USART_DMACmd(USART1, USART_DMAReq_Rx, DISABLE);
}
